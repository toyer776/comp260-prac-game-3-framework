﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Firebullet : MonoBehaviour {
	public BulletMove bulletPrefab;

	// Use this for initialization
	void Start () {
		
	}

	void Update () {
		// when the button is pushed, fire a bullet

		if (Time.timeScale == 0) {
			return;
		}


		if (Input.GetButtonDown("Fire1")) {

			BulletMove bullet = Instantiate(bulletPrefab);
			// the bullet starts at the player's position
			bullet.transform.position = transform.position;

			// create a ray towards the mouse location
			Ray ray = 
				Camera.main.ScreenPointToRay(Input.mousePosition);
			bullet.direction = ray.direction;
		}
	}
}
