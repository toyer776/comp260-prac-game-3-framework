﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShellMenu : MonoBehaviour {

	public GameObject shellPanel;

	public GameObject optionsPanel;
	public Dropdown QualityDropdown;
	public Dropdown ResolutionDropdown;
	public Toggle FullscreenToggle;
	public Slider VolumeSlider;
	public Slider bgmSlider;
	public AudioSource bgmreal;


	private bool paused = true;

	void Start () {
		optionsPanel.SetActive(false);
		SetPaused(paused);

		// populate the list of video quality levels
		QualityDropdown.ClearOptions();
		List<string> names = new List<string>();
		for (int i = 0; i < QualitySettings.names.Length; i++) {
			names.Add(QualitySettings.names[i]);
		}
		QualityDropdown.AddOptions(names);

		// populate the list of available resolutions
		ResolutionDropdown.ClearOptions();
		List<string> resolutions = new List<string>();
		for (int i = 0; i < Screen.resolutions.Length; i++) {
			resolutions.Add(Screen.resolutions[i].ToString());
		}
		ResolutionDropdown.AddOptions(resolutions);


		// restore the saved audio volume
		if (PlayerPrefs.HasKey("AudioVolume")) {
			AudioListener.volume = 
				PlayerPrefs.GetFloat("AudioVolume");
		} else {
			// first time the game is run, use the default value
			AudioListener.volume = 1;
		}

		////////////////////////////////////////////////////

		bgmreal.ignoreListenerVolume = true;

		////////////////////////////////////////////////////

	}

	void Update () {
		// pause if the player presses escape
		if (!paused && Input.GetKeyDown(KeyCode.Escape)) {
			SetPaused(true);
		}
	}

	public void OnPressedOptions() {
		// show the options panel & hide the shell panel
		shellPanel.SetActive(false);
		optionsPanel.SetActive(true);

		// select the current quality value
		QualityDropdown.value = QualitySettings.GetQualityLevel();

		// select the current resolution
		int currentResolution = 0;
		for (int i = 0; i < Screen.resolutions.Length; i++) {
			if (Screen.resolutions[i].width == Screen.width &&
				Screen.resolutions[i].height == Screen.height) {
				currentResolution = i;
				break;
			}
		}
		ResolutionDropdown.value = currentResolution;
		VolumeSlider.value = AudioListener.volume;
		bgmSlider.value = bgmreal.volume;

	}

	public void OnPressedCancel() {
		// return to the shell menu
		shellPanel.SetActive(true);
		optionsPanel.SetActive(false);
	}

	public void OnPressedApply() {
		// apply the changes
		QualitySettings.SetQualityLevel(QualityDropdown.value);

		// return to the shell menu
		shellPanel.SetActive(true);
		optionsPanel.SetActive(false);

		// apply the changes
		QualitySettings.SetQualityLevel(QualityDropdown.value);
		Resolution res =
			Screen.resolutions[ResolutionDropdown.value];
		Screen.SetResolution(res.width, res.height, true);


		Screen.SetResolution(res.width, res.height, 
			FullscreenToggle.isOn);

		AudioListener.volume = VolumeSlider.value;
		PlayerPrefs.SetFloat("AudioVolume", AudioListener.volume);
	}



	private void SetPaused(bool p) {
		paused = p;
		shellPanel.SetActive(paused);
		Time.timeScale = paused ? 0 : 1;
	}


	public void OnPressedPlay() {
		// resume the game
		SetPaused(false);
	}


	public void OnPressedQuit() {
		// quit the game
		Application.Quit();
	}

		
	}

